package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        // Define some prompts and output strings
        String startPrompt = "Your choice (Rock/Paper/Scissors)?";
        String contPrompt = "Do you wish to continue playing? (y/n)?";
        String humanWins = "Human wins!";
        String computerWins = "Computer wins!";
        String tie = "It's a tie!";
        String goodbye = "Bye bye :)";

        // game will run until user says they want to quit
        while (true) {
            // Print round number
            System.out.printf("Let's play round %d%n", roundCounter);

            // Define variable for humans choice
            String humanChoice = "";

            // prompt user choice until users input is valid
            while (true) {
                // Ask for human input
                humanChoice = readInput(startPrompt).toLowerCase();

                // test validity of user input
                boolean isValidChoice = rpsChoices.contains(humanChoice);
                if (!isValidChoice) {
                    System.out.printf("I do not understand %s. Could you try again?%n", humanChoice);
                } else {
                    break;
                }
            }
            int humanChoiceInd = rpsChoices.indexOf(humanChoice);

            // computer chooses at random
            int computerChoiceInd = computerChooses();
            String computerChoice = rpsChoices.get(computerChoiceInd);

            System.out.printf("Human chose %s, computer chose %s. ", humanChoice, computerChoice);
            // Check who wins and update scores
            if (humanChoiceInd == computerChoiceInd) { // Tie
                System.out.println(tie);
            } else if (humanChoiceInd == ((computerChoiceInd + 1) % 3)) { // Human wins
                System.out.println(humanWins);
                humanScore += 1;
            } else { // Computer wins
                System.out.println(computerWins);
                computerScore += 1;
            }
            // Print score
            System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);

            // Ask if user wants to keep playing and stop game if answer is no
            String cont = readInput(contPrompt).toLowerCase();
            if (cont.equals("n") || cont.equals("no")) {
                System.out.println(goodbye);
                break;
            }
            // Update counter for new round
            roundCounter += 1;
            }

    }

    /**
     * Helper function to select computer choice of rock/paper/scissors at random
     * @return random integer to index computer choice
     */
    public Integer computerChooses() {
        Random r = new Random();
        Integer randomInd = r.nextInt(3);
        return randomInd;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
